# About this Project
This Open Source Project is a sample Profile Page Template. It has been created for hosting CyberPhase My Profile Page on the Server.  
Copyright (c) 2021 Atheesh  Thirumalairajan

Project Avatar Forked from https://github.com/elementary/icons

# Dependencies
This project requires the following additional Dependencies (apart from those included):
 - jQuery (https://code.jquery.com/)
 - Bootstrap Material Theme (https://djibe.github.io/material)
 - The Inter Font Family (https://rsms.me/inter/)

# Sample Page
My profile Page on CyberPhase is created using this template at https://cyberphase.tk/@candiedoperation
